import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
// import PrivateRoute from './components/PrivateRoute/PrivateRoute';
import './App.css';
import { routes } from './routes.js';
import LoginPage from './LoginPageModule/LoginPage';
import Stats from './StatsModule/Stats';
import History from './HistoryModule/History';
import TradesModulePage from "./TradesModule/TradesModulePage";


function App() {
  return (
    <Router>
      <Switch>
        <Route path={routes.login} component={LoginPage} />
        <Route path={routes.trades} component={TradesModulePage} />
        <Route path={routes.stats} component={Stats} />
        <Route path={routes.history} component={History} />
      </Switch>
    </Router>
  );
}

export default App;
