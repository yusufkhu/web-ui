import axios from 'axios';
import webService from '../../config';

export const getAllCounterparties = () => {
    return axios.get(`${webService}/getAllCounterparties`)
}

export const getAllInstruments = () => {
    return axios.get(`${webService}/getAllInstruments`)
}

export const getAllDealTypes = () => {
    return axios.get(`${webService}/getAllDealTypes`)
}