import { Observable } from "rxjs";
import webService from '../../config';

export const createStringObservable = (filters) => new Observable(observer => {
    const filterStr = encodeURI(JSON.stringify(filters))
    const source = new EventSource(`${webService}/deals?filters=${filterStr}`);
    source.addEventListener('message', (messageEvent) => {
        console.log(messageEvent);
        observer.next(messageEvent.data);
    }, false);
});
