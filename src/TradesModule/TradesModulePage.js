import React, { useState, useEffect } from 'react';
import Header from '../components/Header/Header';
import FilterPanel from './components/FilterPanel';
import Trades from "./components/Trades";
import { getAllCounterparties, getAllInstruments, getAllDealTypes } from './utils/http';

const TradesModulePage = () => {
    const [counterparties, setCounterparties] = useState([]);
    const [instrumentNames, setInstrumentNames] = useState([]);
    const [dealTypes, setDealTypes] = useState([]);
    const [filters, setFilters] = useState([]);
    // Similar to componentDidMount and componentDidUpdate:
    useEffect(() => {
        getAllInstruments().then(resp2 =>{ setInstrumentNames(resp2.data.data)});
        getAllCounterparties().then(resp => {setCounterparties(resp.data.data); }); 
        getAllDealTypes().then( resp3 => {setDealTypes(resp3.data.data); });
    }, []);
    const receiveFilters = childData => {
        setFilters(childData)
    }
    return (
        <>
            <Header />
            <FilterPanel counterparties={counterparties} instrumentNames={instrumentNames} dealTypes={dealTypes} passFilters={receiveFilters}/>
            <Trades filters={filters}/>
        </>
    );
}

export default TradesModulePage;
