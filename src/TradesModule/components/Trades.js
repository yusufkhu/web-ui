import React, { useState } from 'react';
import { useObservable } from 'rxjs-hooks';
import { map, withLatestFrom } from 'rxjs/operators';
import { createStringObservable } from "../utils/observable";
import Table from './Table';

const stopTrading = () => {

}
const Trades = (props) => {
    console.log(props.filters);
    const [stringArray, setStringArray] = useState([]);
    useObservable(
        state =>
            createStringObservable(props.filters).pipe(
                withLatestFrom(state),
                map(([state]) => {
                    let updatedStringArray = stringArray;
                    updatedStringArray.unshift(state);
                    if (updatedStringArray.length >= 50) {
                        updatedStringArray.pop();
                    }
                    setStringArray(updatedStringArray);
                    return state;
                })
            )
    );

    return (
        <>
            <Table data={stringArray} />
            {/* <Graph stringArray={stringArray} /> */}
            {/*{stringArray ? stringArray.map((message, index) => <p key={index}>{message}</p>) : <p>Loading...</p>}*/}
        </>
    );
}

export default Trades;
