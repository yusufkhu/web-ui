import React from 'react';
import '../styles/TableRow.css'

const TableRow = (props) => {
    return (
        <div className='table-row'>
            <span>
                {props.row.instrumentName}
            </span>
            <span>
                {props.row.cpty}
            </span>
            <span>
                {props.row.price}
            </span>
            <span>
                {props.row.type}
            </span>
            <span>
                {props.row.quantity}
            </span>
            <span>
                {props.row.time}
            </span>
        </div>
    )
}

export default TableRow