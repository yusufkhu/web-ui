import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

const FilterPanel = (props) => {
    // const [counterparties, setCounterparties] = useState([]);
    // const [instrumentNames, setInstrumentNames] = useState([]);
    // const [dealTypes, setDealTypes] = useState([]);
    // // Similar to componentDidMount and componentDidUpdate:
    // useEffect(() => {
    //     getAllInstruments().then(resp2 =>{ setInstrumentNames(resp2.data.data)});
    //     getAllCounterparties().then(resp => {setCounterparties(resp.data); }); 
    //     getAllDealTypes().then( resp3 => {setDealTypes(resp3.data); });
    // }, []);
    // console.log(counterparties, instrumentNames, dealTypes);
    const handleSubmit = event => {
        event.preventDefault();
        const form = event.currentTarget;
        let filters = [];
        const fieldsMap = {0:'cpty', 1:'instrumentName', 2:'type'}
        for (var i =0; i<3; i++){
            if(form.elements[i].value !== 'Choose...'){
                filters.push({
                    field:fieldsMap[i],
                    value:form.elements[i].value,
                    exp:'=='
                })
            }
        }
        props.passFilters(filters)
        console.log(form.elements[0].value)
        }
    let cptyArr = props.counterparties.length === 0? [] :props.counterparties.map(element => <option key={`counterparty_${element.id}`}>{element.name}</option>);
    let instrArr = props.instrumentNames.length === 0? [] : props.instrumentNames.map(element => <option key={`instrument_${element.id}`}>{element.name}</option>);
    let dealArr = props.dealTypes.length === 0? [] :  props.dealTypes.map(element => <option key={`dealType_${element.id}`}>{element.name}</option>);
    return (
        <Form onSubmit={handleSubmit}>
            <Form.Row>
                <Form.Group as={Col} controlId="formGridState">
                    <Form.Label>Select counterparty:</Form.Label>
                    <Form.Control as="select">
                        <option>Choose...</option>
                        {cptyArr}
                    </Form.Control>
                </Form.Group>

                <Form.Group as={Col} controlId="formGridState">
                    <Form.Label>Select instrument:</Form.Label>
                    <Form.Control as="select">
                        <option>Choose...</option>
                        {instrArr}
                    </Form.Control>
                </Form.Group>

                <Form.Group as={Col} controlId="formGridZip">
                <Form.Label>Select deal type:</Form.Label>
                    <Form.Control as="select">
                        <option>Choose...</option>
                        {dealArr}
                    </Form.Control>
                </Form.Group>
                <Button variant="primary" type="submit" className='filter-button'>
                        Filter!
                    </Button>
            </Form.Row>
        </Form>
    )
}

export default FilterPanel
