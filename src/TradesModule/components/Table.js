import React from 'react';
import TableRow from './TableRow';
import { calculateTradeKey } from '../utils/calculateTradeKey';
const Table = (props) => {
    const defaultRow = {
        instrumentName: 'Instrument name', cpty: 'Counterparty',
        price: 'Price', type: 'Type', quantity: 'Quantity', time: 'Time'
    }
    // let initArray = [<TableRow row={defaultRow} />]
    // let rows = initArray.concat(props.data.map(element => <TableRow key={calculateTradeKey(JSON.parse(element))} row={JSON.parse(element)} />))
    return (
        <>
            <TableRow row={defaultRow} />
            {props.data.map(element => <TableRow key={calculateTradeKey(JSON.parse(element))} row={JSON.parse(element)} />)}
        </>
    )
}

export default Table;