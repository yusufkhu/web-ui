import React, { useState } from 'react';
import Line from 'react-chartjs-2';
import { defaults } from 'react-chartjs-2';
import { union } from 'lodash';

const Graph = (props) => {
    // let [prevArray, setPrevArray] = useState([]);
    // if (prevArray !== props.stringArray) {
    //     let new_arr = union(props.stringArray, prevArray)
    //     setPrevArray(new_arr)
    // }

    console.log(props.stringArray)
    if (!props.stringArray) {
        return null
    }
    // const stringArray = JSON.parse(props.stringArray)
    const labels = props.stringArray.map(element => element.time)
    const x = props.stringArray.map(element => element.price)
    const data = {
        labels: labels,
        datasets: [
            {
                label: 'My First dataset',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: x
            }
        ]
    };
    return (
        <Line data={data} />
    )
}
export default Graph;