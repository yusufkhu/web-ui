import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { authRequest } from './utils/http';
import './LoginPage.css';
import { routes } from '../routes';


class LoginPage extends React.Component {
    state = {
        validated: false,
    }

    handleSubmit = event => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        authRequest(form.elements[0].value, form.elements[1].value).then(
            (response) => {
                if (response.status === 200) {
                    const data = response.data;
                    sessionStorage.setItem('role', data.role);
                    sessionStorage.setItem('isAuthorized', true);
                    switch (data.role) {
                        case 'trader': this.props.history.push(routes.trades); break;
                        case 'security': this.props.history.push(routes.dbhealth); break;
                        default: break;
                    }
                }
            }
        );
        // if (response.status == 200) {
        //     this.setState({ validated: true });
        //     console.log('success!!!');
        // }

    };

    render() {
        return (
            <div className='form-wrapper'>
                <Form noValidate validated={this.state.validated} onSubmit={this.handleSubmit}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" ref="email" placeholder="Enter email" required />
                        <Form.Control.Feedback type="invalid">
                            Please enter a valid email
                        </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" ref="password" placeholder="Password" required />
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Login
                    </Button>
                </Form>
            </div>
        )
    }
}

export default LoginPage