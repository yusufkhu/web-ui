import axios from 'axios';
import webServer from '../config';

export const getAverages = () => {
    return axios.get(`${webServer}/getAverage`)
}