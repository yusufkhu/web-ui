import React, {useState} from 'react';
import Header from '../components/Header/Header';
import Button from 'react-bootstrap/Button';
import {getAverages} from './http';

const Stats = () => {
    const [stats, setStats] = useState({});
    const handleSubmit = () => {
    getAverages().then(
        response=> setStats(response.data))
    }
    return (
        <div>
            <Header />
            <Button onClick={handleSubmit}>GET STATS!</Button>
            <p>{JSON.stringify(stats)}</p>
        </div>
    )
}
export default Stats;