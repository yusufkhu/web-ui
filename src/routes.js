export const routes = {
    root: '/',
    login: '/login',
    trades: '/trades',
    stats: '/stats',
    history: '/history',
    dbhealth: '/dbhealth',
    users: '/users'
}