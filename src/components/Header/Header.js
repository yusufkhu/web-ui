import React from 'react';
import { NavLink } from 'react-router-dom';
// import Trades from '../../TradesModule/Trades';
// import History from '../../HistoryModule/History';
// import Stats from '../../StatsModule/Stats';
import { routes } from '../../routes';
import './Header.css';

const Header = () => {
    return (
        <div className='nav-wrapper'>
            <NavLink to={routes.trades} className="nav-link" activeClassName="nav-link active">Trades</NavLink>
            <NavLink to={routes.stats} className="nav-link" activeClassName="nav-link active">Stats</NavLink>
            <NavLink to={routes.history} className="nav-link" activeClassName="nav-link active">History</NavLink>
        </div>
    )
}

export default Header;